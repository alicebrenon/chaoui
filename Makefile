JS_SRCDIR=js
CSS_SRCDIR=css
JS_SRC=$(shell find $(JS_SRCDIR) -type f -name '*.js')
CSS_SRC=$(shell find $(CSS_SRCDIR) -type f -name '*.css')
LIB=unitJS
JS=main.js
CSS=style.css
TARGET=$(JS) $(CSS)

all: $(TARGET)

$(JS): $(JS_SRC)
	sjw $(LIB:%=-I %) -o $@ $(JS_SRCDIR)

$(CSS): $(CSS_SRC)
	cat $^ > $@

mrproper:
	rm $(TARGET)
