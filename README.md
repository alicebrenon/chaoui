# Chaoui

Chaoui is a web application designed to render ALTO files and to edit them with a simple graphical interface. Its usage is documented in the [wiki](https://gitlab.huma-num.fr/alicebrenon/chaoui/-/wikis).

## Install

It is a full client-side application. It doesn't need any server backend and can be deployed anywhere that's able to serve static files. You only need to assemble the sources contained in this repository once and then the site can be deployed wherever you want by means of a `rsync`, a `FTP` file transfer or any other protocol of your choice.

### Dependencies

#### [SJW](https://git.marvid.fr/Tissevert/SJW)

Chaoui is written as a modular SJW application so that tool is needed to «compile» the Javascript code into the single `main.js` file. Make sure you have it installed and `sjw` is found in your `$PATH`.

```
$ sjw --help
```

#### [UnitJS](https://git.marvid.fr/Tissevert/UnitJS)

The Javascript code makes use of the `UnitJS` library so you need it installed as a `SJW` package. Make sure it is installed where `sjw` can find it, for example :

```
$ ls ~/.sjw | grep unitJS
unitJS
```

Note that `SJW` «compiles» all the required Javascript code into the output file so the library `UnitJS` is only needed on the machine where you assemble `chaoui`, not on the server. This behaviour is analogous to `static linking` in the world of binary executables.

#### [make](https://www.gnu.org/software/make/)

Assembling the application requires a call to `sjw` and packing the CSS code with `cat`. Both operations are implemented by the `Makefile` in this repository so having the `make` tool installed is recommended though optional. The content of the `Makefile` documents the two lines you need to type (a call to `sjw` and a call to `cat`) if you can't or don't want to use `make`.

### Assembling the application

Assuming you have all the dependencies above installed on your system, just open a shell at the root of this repository and type :

```
$ make
```

This will generate two files, `main.js` and `style.css`, at the root of the repository.

```
$ ls
css	index.html  js	main.js  Makefile  README.md  style.css
```

### Deployment

Just copy `index.html`, `main.js` and `style.css` wherever you want your web server to find them. For example assuming you have a SSH access to `yourhost.net` and you want to deploy `chaoui` to the path `web/chaoui` on thas host you could type something like :

```
$ rsync -aHP index.html main.js style.css yourhost.net:web/chaoui/
```

Note that if you just need to use `chaoui` yourself and don't need to make it available to a team, you can also simply assemble it on your host and then open `index.html` in your web browser.
