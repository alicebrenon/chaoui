import File;
import GUI;
import push from GUI.Editor;
import * as Keys from GUI.Keys;
import Scoria;
import * as Zoom from Toolbar.Zoom;
import * as Async from UnitJS.Async;
import XML;
import * as ALTO from XML.ALTO;

var currentALTOFile = null;
GUI.wcThreshold.addEventListener('change', refresh);
GUI.loadScoria.addEventListener('click', importScoria);
GUI.saveScoria.addEventListener('click', exportScoria);

GUI.fitWidth.addEventListener('click', scale({fitWidth: true}));
GUI.zoomAmount.addEventListener('change', scale(function(x) {return x;}));
GUI.fitHeight.addEventListener('click', scale({fitHeight: true}));
Keys.bind('w', scale({fitWidth: true}));
Keys.bind('h', scale({fitHeight: true}));
Keys.bind('+', scale(function(x) {return Math.min(GUI.zoomAmount.max, x + 10);}));
Keys.bind('-', scale(function(x) {return Math.max(GUI.zoomAmount.min, x - 10);}));

return {
	open: open
};

function open(file) {
	Async.run(
		Async.bind(
			File.load(file),
			Async.map(XML.parse),
			Async.map(function(ALTOFile) {
				currentALTOFile = ALTOFile;
				refresh();
			})
		)
	);
}

function refresh() {
	if(currentALTOFile != undefined) {
		ALTO.display(currentALTOFile);
	}
}

function scale(o) {
	return function() {
		if(typeof o == 'function') {
			o.zoom = GUI.zoomAmount.value = o(1*GUI.zoomAmount.value);
		}
		Zoom.setScalingMode(o);
		refresh();
	}
}

function importScoria(e) {
	if(!GUI.loadScoria.classList.contains('disabled')) {
		Async.run(
			Async.bind(
				File.pick({accept:"text/csv,.csv", multiple: true}),
				function(input) {
					var loaders = [];
					for(var i = 0; i < input.files.length; i++) {
						loaders.push(File.load(input.files[i]));
					}
					return Async.parallel.apply(null, loaders);
				},
				Async.map(setFromFiles),
				Async.map(refresh)
			)
		);
	} else {
		e.stopPropagation();
	}
}

function setFromFiles(files) {
	var edit = [];
	files.forEach(function(file) {
		var wordIds = file.split('\n').slice(1);
		wordIds.forEach(function(line) {
			Scoria.setScoria(line);
		});
		edit = edit.concat(wordIds);
	});
	push('le chargement des ' + files.length + ' fichiers de scories', edit);
}

function exportScoria(e) {
	if(!GUI.saveScoria.classList.contains('disabled')) {
		var column = ['ID'].concat(Scoria.getScoriae());
		var data = 'data:text/csv,' + encodeURIComponent(column.join('\n'));
		File.save(data, 'scoria.csv');
	} else {
		e.stopPropagation();
	}
}
