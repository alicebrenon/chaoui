import * as Segment from Geometry.Segment;

return {
	contains: contains,
	intersects: intersects,
	make: make
}

function make(o) {
	if(o.x != undefined && o.y != undefined) {
		return {x: Segment.make(o.x), y: Segment.make(o.y)};
	} else if(o instanceof HTMLElement) {
		var bounds = o.getBoundingClientRect();
		return {
			x: Segment.make({from: bounds.x, to: bounds.right}),
			y: Segment.make({from: bounds.y, to: bounds.bottom})
		}
	}
}

function contains(a, b) {
	return Segment.contains(a.x, b.x) && Segment.contains(a.y, b.y);
}

function intersects(a, b) {
	return Segment.intersects(a.x, b.x) && Segment.intersects(a.y, b.y);
}
