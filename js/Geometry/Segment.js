return {
	contains: contains,
	intersects: intersects,
	make: make,
	oNorm: oNorm
}

function make(o) {
	if(o.origin != undefined && o.norm != undefined) {
		return {min: o.origin, max: o.origin + o.norm};
	} else if(o.from != undefined && o.to != undefined) {
		return {min: Math.min(o.from, o.to), max: Math.max(o.from, o.to)};
	}
}

function oNorm(o, norm) {
	return {o: 1*o + Math.min(0, 1*norm), norm: Math.abs(1*norm)};
}

function contains(segmentA, segmentB) {
	return segmentA.min <= segmentB.min && segmentB.max <= segmentA.max;
}

function intersects(segmentA, segmentB) {
	return segmentA.min <= segmentB.min && segmentB.min <= segmentA.max
		|| segmentB.min <= segmentA.min && segmentA.min <= segmentB.max;
}
