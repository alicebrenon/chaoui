import scale from Toolbar.Zoom;
import oNorm from Geometry.Segment;

var schema = {
	'Page': {childTags: ['PrintSpace']},
	'PrintSpace': {childTags: ['TextBlock', 'Illustration'], htmlTag: 'div'},
	'TextBlock': {childTags: ['TextLine'], htmlTag: 'div'},
	'Illustration': {htmlTag: 'div'},
	'TextLine': {childTags: ['String'], htmlTag: 'p'},
	'String': {htmlTag: 'span'}
};

return {
	make: make,
	schema: schema
};

function make(xmlElement) {
	var x = oNorm(get('HPOS'), get('WIDTH'));
	var y = oNorm(get('VPOS'), get('HEIGHT'));

	return {
		childTags: schema[xmlElement.tagName].childTags,
		geometry: {
			hPos: x.o,
			vPos: y.o,
			width: x.norm,
			height: y.norm
		},
		get: get,
		htmlTag: schema[xmlElement.tagName].htmlTag,
		id: get('ID'),
		position: position,
		tag: xmlElement.tagName
	};

	function get(attribute) {
		return xmlElement.getAttribute(attribute);
	}

	function position(domElement, parentElement) {
		var mother = parentElement ? parentElement.geometry : {hPos: 0, vPos: 0};
		scale(domElement, {
			left: x.o - mother.hPos, /* due to absolute positioning in CSS, this needs to be */
			top: y.o - mother.vPos, /* computed as an offset from the parentElement */
			width: x.norm,
			height: y.norm
		}, 'left', 'top', 'width', 'height')
	}
}
