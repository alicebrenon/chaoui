import * as Element from XML.ALTO.Element;
import isScoria from Scoria;
import Quality;
import GUI;
import push from GUI.Editor;
import * as Dom from UnitJS.Dom;
import * as Zoom from Toolbar.Zoom;

var blockPosition;

return {
	edit: edit,
	display: display
};

function display(ALTODoc) {
	var page = ALTODoc.querySelector('Page');
	blockPosition = 0;
	Zoom.scaleTo(Element.make(page).geometry);
	Dom.clear(GUI.page);
	GUI.page.appendChild(makeDomNode(page.querySelector('PrintSpace')));
}

function makeDomNode(xmlElement, parentElement) {
	var element = Element.make(xmlElement);
	var attributes = {class: [element.tag], id: element.get('ID')};
	if(element.tag == 'String') {
		setAttributes(element, attributes);
	} else {
		var children = makeChildren(xmlElement, element);
	}
	var dom = Dom.make(element.htmlTag, attributes, children);
	element.position(dom, parentElement);
	return dom;
}

function makeChildren(xmlElement, element) {
	var children = [];
	if(Element.schema.PrintSpace.childTags.indexOf(element.tag) >= 0) {
		children.push(blockPositionElem());
	}
	if(Array.isArray(element.childTags)) {
		xmlElement.querySelectorAll(element.childTags.join(', ')).forEach(
			function(child) {
				children.push(makeDomNode(child, element));
			}
		);
	}
	return children;
}

function setAttributes(element, attributes) {
	attributes.textContent = element.get('CONTENT');
	attributes.class = attributes.class
		.concat(Quality.isLow(element.get('WC')) ? 'lowQuality' : [])
		.concat(isScoria(element.get('ID')) ? 'deleted' : []);
	attributes.onClick = function(e) {
		if(GUI.mode.value == 'edit') {
			var word = e.target;
			var action = isScoria(word.id) ? "l'ajout" : "la suppression";
			push(action + ' du mot «' + word.textContent + '»', [word.id], true);
		}
	};
}

function blockPositionElem() {
	return Dom.make('div', {class: 'BlockPosition'}, [
		Dom.make('div', {textContent: blockPosition++})
	]);
}

function edit(ALTODoc) {
	var printSpace = ALTODoc.querySelector('Page PrintSpace');
	editElement(printSpace);
	return ALTODoc;
}

function editElement(xmlElement) {
	var element = Element.make(xmlElement);
	if(element.tag == 'String') {
		return !isScoria(element.get('ID'));
	} else {
		editChildren(xmlElement, element.childTags);
		return xmlElement.children.length > 0;
	}
}

function editChildren(xmlElement, childTags) {
	for(var i = 0; i < xmlElement.children.length; i++) {
		var child = xmlElement.children[i];
		if(childTags.indexOf(child.tagName) >= 0 && !editElement(child)) {
			xmlElement.removeChild(child);
		}
	}
}
