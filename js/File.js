import * as Dom from UnitJS.Dom;

return {
	load: load,
	pick: pick,
	save: save
};

function pick(attributes) {
	return function(f) {
		attributes.type = 'file';
		var input = Dom.make('input', attributes);
		input.addEventListener('change', function() {
			f(input);
		});
		input.click();
	}
}

function load(file) {
	return function(f) {
		var fileReader = new FileReader();
		fileReader.addEventListener('load', function() {
			f(fileReader.result);
		})
		fileReader.readAsText(file);
	};
}

function save(data, name) {
	var a = Dom.make('a', {download: name, href: data});
	a.click();
}
