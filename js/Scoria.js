import File;

var scoriae = {};

return {
	getScoriae: getScoriae,
	isScoria: isScoria,
	setScoria: setScoria,
	toggleWord: toggleWord
};

function getScoriae() {
	var result = [];
	for(var id in scoriae) {
		result.push(id);
	}
	return result;
}

function isScoria(wordId) {
	return scoriae[wordId] || false;
}

function setScoria(wordId, state) {
	if(state == undefined || state) {
		scoriae[wordId] = true;
	} else {
		delete scoriae[wordId];
	}
}

function toggleWord(wordId) {
	var state = !scoriae[wordId];
	setScoria(wordId, state);
	var word = document.getElementById(wordId);
	if(word != undefined) {
		word.classList.toggle('deleted', state);
	}
}
