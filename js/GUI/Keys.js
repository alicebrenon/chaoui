var bindings = binTree(4);

return {
	bind: bind,
	init: init
};

function binTree(n) {
	if(n == 0) {
		return {};
	} else {
		return {false: binTree(n-1), true: binTree(n-1)};
	}
}

function bind(keyCode, f) {
	var components = keyCode.match(/((?:Alt|Ctrl|Meta|Shift)\+)*(.+)$/);
	if(components) {
		var key = components[2];
		var modifiers = components[0].split('+');
		bindings
			[modifiers.includes('Alt')]
			[modifiers.includes('Ctrl')]
			[modifiers.includes('Meta')]
			[modifiers.includes('Shift')]
			[key] = f;
	} else {
		console.log("Did not add binding for invalid key code '" + keyCode + "'");
	}
}

function init() {
	window.addEventListener('keydown', function(e) {
		var binding = bindings[e.altKey][e.ctrlKey][e.metaKey][e.shiftKey][e.key];
		if(binding != undefined) {
			binding();
		}
	});
}
