import GUI;
import bind from GUI.Keys;
import toggleWord from Scoria;

var edits = [];
var cursor = -1;

GUI.cancel.addEventListener('click', cancel);
GUI.editCancel.addEventListener('click', cancel);
GUI.redo.addEventListener('click', redo);
GUI.editRedo.addEventListener('click', redo);
bind('Ctrl+z', cancel);
bind('Ctrl+y', redo);

return {
	push: push
};

function cancel(e) {
	if(cursor >= 0) {
		edits[cursor--].wordIds.forEach(function(wordId) {toggleWord(wordId);});
		updateButtons();
	} else if(e != undefined) {
		e.stopPropagation();
	}
}

function redo(e) {
	if(cursor < edits.length - 1) {
		edits[++cursor].wordIds.forEach(function(wordId) {toggleWord(wordId);});
		updateButtons();
	} else if(e != undefined) {
		e.stopPropagation();
	}
}

function push(description, wordIds, perform) {
	edits.length = ++cursor + 1;
	edits[cursor] = {description: description, wordIds: wordIds};
	if(perform) {
		wordIds.forEach(function(wordId) {toggleWord(wordId);});
	}
	updateButtons();
}

function setState(action, disabled) {
	if(action == 'redo') {
		var label = 'Refaire' + (disabled ? '' : ' ' + edits[cursor + 1].description);
	} else {
		var label = 'Annuler' + (disabled ? '' : ' ' + edits[cursor].description);
	}
	GUI[action].title = label;
	GUI[action].disabled = disabled;
	var menuElem = GUI['edit' + action[0].toUpperCase() + action.slice(1)];
	menuElem.textContent = label;
	menuElem.classList.toggle('disabled', disabled);
}

function updateButtons() {
	setState('cancel', cursor < 0);
	setState('redo', cursor >= edits.length - 1);
}
