import wcThreshold from GUI;

return {
	isLow: isLow
};

function isLow(quality) {
	return quality < wcThreshold.value;
}
