return {
/* Display */
	windowCatchAll: document.getElementById('windowCatchAll'),
	workzone: document.getElementById('workzone'),
	page: document.getElementById('page'),
	selector: document.getElementById('selector'),

/** Toolbar **/
/* Menus */
	menus: document.getElementsByClassName('menu'),
	fileMenu: document.getElementById('file'),
	loadALTO: document.getElementById('loadALTO'),
	saveALTO: document.getElementById('saveALTO'),
	loadScoria: document.getElementById('loadScoria'),
	saveScoria: document.getElementById('saveScoria'),

	editMenu: document.getElementById('edit'),
	editCancel: document.getElementById('editCancel'),
	editRedo: document.getElementById('editRedo'),

	helpMenu: document.getElementById('help'),
	documentation: document.getElementById('documentation'),
	reposLink: document.getElementById('reposLink'),

/* Controls */
	fileNumber: document.getElementById('fileNumber'),
	fileName: document.getElementById('fileName'),
	wcThreshold: document.getElementById('wcThreshold'),
	mode: document.getElementById('mode'),

/* Cancel / Redo */
	cancel: document.getElementById('cancel'),
	redo: document.getElementById('redo'),

/* Zoom */
	zoom: document.getElementById('zoom'),
	fitWidth: document.getElementById('fitWidth'),
	zoomAmount: document.getElementById('zoomAmount'),
	fitHeight: document.getElementById('fitHeight')
};
