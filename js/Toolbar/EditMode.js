import {mode, page, selector, workzone} from GUI;
import GUI.Editor;
import * as Rectangle from Geometry.Rectangle;
import toggleWord from Scoria;

var currentMode = null;

var selection = {
	words: {},
	x: {from: null, to: null},
	y: {from: null, to: null}
};

mode.addEventListener('change', sync);
workzone.addEventListener('mousedown', startRectangle);
workzone.addEventListener('mousemove', moveRectangle);
workzone.addEventListener('mouseup', endRectangle);

return {
	sync: sync
};

function sync() {
	page.classList.remove(currentMode);
	currentMode = mode.value + 'Mode';
	page.classList.add(currentMode);
}

function startRectangle(e) {
	if(mode.value == 'edit') {
		selection.x.from = e.clientX;
		selection.y.from = e.clientY;
		selector.className = 'on';
		page.classList.add('freeze');
	}
}

function moveRectangle(e) {
	if(selection.x.from != undefined) {
		selection.x.to = e.clientX;
		selection.y.to = e.clientY;
		selector.style.left = Math.min(selection.x.from, selection.x.to) + 'px';
		selector.style.width = Math.abs(selection.x.to - selection.x.from) + 'px';
		selector.style.top = Math.min(selection.y.from, selection.y.to) + 'px';
		selector.style.height = Math.abs(selection.y.to - selection.y.from) + 'px';
		updateSelection();
	}
}

function endRectangle(e) {
	if(mode.value == 'edit') {
		recordEdit();
		selection = {words: {}, x: {from: null, to: null}, y: {from: null, to: null}};
		selector.style.left = selector.style.top = selector.style.width =
			selector.style.height = null;
		selector.className = '';
		page.classList.remove('freeze');
	}
}

function updateSelection() {
	var rectangle = Rectangle.make(selection);
	updateSelected(rectangle, page);
	for(var id in selection.words) {
		if(!Rectangle.contains(rectangle, selection.words[id])) {
			toggleWord(id);
			delete selection.words[id];
		}
	}
}

function updateSelected(rectangle, domElem) {
	if(domElem.classList.contains('String')) {
		updateWord(rectangle, domElem);
	} else {
		for(var i = 0; i < domElem.children.length; i++) {
			if(Rectangle.intersects(rectangle, Rectangle.make(domElem.children[i]))) {
				updateSelected(rectangle, domElem.children[i]);
			}
		}
	}
}

function updateWord(rectangle, domElem) {
	var elemRectangle = Rectangle.make(domElem);
	if(Rectangle.contains(rectangle, elemRectangle) && !selection.words[domElem.id]) {
		selection.words[domElem.id] = elemRectangle;
		toggleWord(domElem.id);
	}
}

function recordEdit() {
	var edit = Object.keys(selection.words);
	if(edit.length > 0) {
		GUI.Editor.push('la sélection rectangulaire de ' + edit.length + ' mots', edit);
	}
}
