import {menus, windowCatchAll} from GUI;
import * as Keys from GUI.Keys;

var selectedMenu = null;

Keys.bind('Escape', closeMenu);
windowCatchAll.addEventListener('click', closeMenu);

return {
	init: init
}

function init() {
	for(var i = 0; i < menus.length; i++) {
		menus[i].querySelector('span').addEventListener('click', toggleMenu(menus[i]));
		menus[i].querySelector('span').addEventListener('mouseenter', openMenu(menus[i]));
		menus[i].querySelector('ul').addEventListener('click', closeMenu);
	}
}

function openMenu(menu) {
	return function() {
		if(selectedMenu != undefined && selectedMenu != menu) {
			menu.classList.add('open');
			selectedMenu.classList.remove('open');
			selectedMenu = menu;
		}
	}
}

function toggleMenu(menu) {
	return function() {
		if(selectedMenu == undefined) {
			menu.classList.add('open');
			selectedMenu = menu;
			windowCatchAll.className = 'on';
		} else {
			closeMenu();
		}
	};
}

function closeMenu(e) {
	if(selectedMenu != undefined) {
		selectedMenu.classList.remove('open');
		selectedMenu = null;
		windowCatchAll.className = '';
	}
}
