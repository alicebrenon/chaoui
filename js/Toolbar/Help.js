import {documentation, reposLink} from GUI;

return {
	init: init
};

function init() {
	[documentation, reposLink].forEach(function(elem) {
		elem.addEventListener('click', function(e) {
			if(e.target.tagName != 'A') {
				e.target.children[0].click();
			}
		});
	});
}
