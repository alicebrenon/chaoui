import GUI;

var initialRatio = 0.25;
var initialFontSize = 9;
var scalingMode = initialRatio;
var ratio = initialRatio;

setScalingMode(scalingMode);

return {
	scale: scale,
	scaleTo: scaleTo,
	setScalingMode: setScalingMode
};

function className() {
	if(typeof scalingMode == 'number') {
		return 'ratio';
	} else {
		return scalingMode;
	}
}

function scaleTo(geometry) {
	switch(scalingMode) {
		case 'fitWidth':
			GUI.page.style.width = '100%';
			ratio = GUI.page.clientWidth / geometry.width;
			scale(GUI.page, geometry, 'height');
			GUI.zoomAmount.value = 10 * Math.round(10 * ratio / initialRatio);
			break;
		case 'fitHeight':
			GUI.page.style.height = '100%';
			ratio = GUI.page.clientHeight / geometry.height;
			scale(GUI.page, geometry, 'width');
			GUI.zoomAmount.value = 10 * Math.round(10 * ratio / initialRatio);
			break;
		default:
			ratio = scalingMode;
			scale(GUI.page, geometry, 'height', 'width');
	}
	var fontSize = Math.round(initialFontSize * ratio / initialRatio);
	GUI.page.style['font-size'] = fontSize + 'px';
}

function setScalingMode(mode) {
	GUI.page.classList.remove(className());
	if(mode.zoom != undefined) {
		scalingMode = mode.zoom * initialRatio / 100;
	} else if(mode.fitWidth) {
		scalingMode = 'fitWidth';
	} else if(mode.fitHeight) {
		scalingMode = 'fitHeight';
	}
	GUI.page.classList.add(className());
}

function scale(element, geometry) {
	for(var i = 2; i < arguments.length; i++) {
		var pxSize = Math.round(geometry[arguments[i]] * ratio)
		element.style[arguments[i]] = pxSize + 'px';
	}
}
