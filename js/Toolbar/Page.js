import File;
import * as GUI from GUI;
import * as Keys from GUI.Keys;
import * as Async from UnitJS.Async;
import XML;
import * as ALTO from XML.ALTO;
import View;

var files = null;

GUI.loadALTO.addEventListener('click', function() {
	Async.run(
		Async.bind(
			File.pick({accept:"text/xml,.xml", multiple: true}),
			function(input) {
				files = input.files;
				GUI.fileNumber.value = 0;
				syncNumber();
				updateNumber();
				['loadScoria', 'saveScoria', 'saveALTO'].forEach(function(elem) {
					GUI[elem].classList.remove('disabled');
				});
				return Async.wrap();
			}
		)
	);
});

GUI.saveALTO.addEventListener('click', function(e) {
	if(!GUI.saveALTO.classList.contains('disabled')) {
		var file = files[GUI.fileNumber.value];
		Async.run(
			Async.bind(
				File.load(file),
				Async.map(XML.parse),
				Async.map(function(ALTOFile) {
					var payload = encodeURIComponent(XML.serialize(ALTO.edit(ALTOFile)));
					File.save('data:text/xml,' + payload, file.name);
				})
			)
		);
	} else {
		e.stopPropagation();
	}
});

GUI.fileNumber.addEventListener('change', updateNumber);
Keys.bind('ArrowLeft', function() {moveFile(-1)});
Keys.bind('ArrowRight', function() {moveFile(+1)});

return {
	syncNumber: syncNumber
}

function syncNumber() {
	if(files != null) {
		GUI.fileNumber.max = files.length-1;
		GUI.fileName.textContent = files[GUI.fileNumber.value].name;
	} else {
		GUI.fileNumber.max = 0;
	}
}

function updateNumber() {
	if(GUI.fileNumber.checkValidity() && files != undefined) {
		gotoFile(GUI.fileNumber.value);
	}
}

function moveFile(delta) {
	var n = 1*GUI.fileNumber.value + delta;
	if(n >= GUI.fileNumber.min && n <= GUI.fileNumber.max) {
		GUI.fileNumber.value = n;
		gotoFile(n);
	}
}

function gotoFile(n) {
	GUI.fileName.textContent = files[n].name;
	View.open(files[n]);
}
