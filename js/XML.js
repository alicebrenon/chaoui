return {
	parse: parse,
	serialize: serialize
}

function parse(input) {
	var domReader = new DOMParser();
	return domReader.parseFromString(input, 'text/xml');
}

function serialize(xmlDoc) {
	var serializer = new XMLSerializer();
	return serializer.serializeToString(xmlDoc);
}
