import * as Keys from GUI.Keys;
import * as EditMode from Toolbar.EditMode;
import * as Help from Toolbar.Help;
import * as Menu from Toolbar.Menu;
import * as Page from Toolbar.Page;

EditMode.sync();
Page.syncNumber();
Help.init();
Menu.init();
Keys.init();
